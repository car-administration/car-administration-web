from typing import Union
import pytest


# define the functions
def one_and_one():
    return 2


def calc_contents(width: Union[float, int],
                  height: Union[float, int],
                  length: Union[float, int]) -> float:
    '''
    Calculates the size of an object
    '''
    # verify the types
    allowed_types = [int, float]

    if type(width) not in allowed_types:
        raise ValueError(f"The width has the wrong type {type(width)}")

    size = width * length * height

    return size


# write the tests
def test_one_and_one():
    assert one_and_one() == 2


def test_calc_contents():
    assert calc_contents(2, 2, 2) == 8
    assert calc_contents(10, 10, 10) == 1000
    assert calc_contents(10, 10, 10) == 1000


def test_one_and_one_wrong():
    with pytest.raises(TypeError):
        assert one_and_one(5) == 2


def test_calc_contents_wrong():
    with pytest.raises(ValueError):
        calc_contents("10", 10, 10)
