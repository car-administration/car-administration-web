from flask import Flask, render_template
import requests

app = Flask(__name__)


@app.route("/")
def index():
    return "Vroom vroom 🚗🚗. This is now complete."


@app.route("/view_car/<brand>")
def show_car(brand):
    # uppercase the brand
    brand_upper = brand.upper()

    # specify the endpoint
    endpoint = f"https://opendata.rdw.nl/resource/m9d7-ebf2.json?merk={brand_upper}"

    # execute the request
    response = requests.get(endpoint)

    # get the body of the response
    data = response.json()

    # show the first cars in the list
    data_sub = data[:5]

    return render_template("show_car.html", data_sub=data_sub)


if __name__ == "__main__":
    app.run(host="0.0.0.0")
